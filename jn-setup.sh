#!/usr/bin/env sh

set -ex

#### quick things...

sed -i -e 's/^SYSLOGD_OPTS=.*/SYSLOGD_OPTS="-t -s 1024"/' "$TARGET/etc/conf.d/syslog"

cp -a ./setup "$TARGET/setup"
chroot "$TARGET" /setup/preload-bootstrap.sh
chroot "$TARGET" /setup/config-docker.sh

### more-complex thing...

chroot "$TARGET" /setup/install-efs-utils.sh

### very complex thing...

./setup/install-aws2.sh

### and lastly...

rm -rf "$TARGET/setup"

# what's / look like?
ls -alF "$TARGET"
