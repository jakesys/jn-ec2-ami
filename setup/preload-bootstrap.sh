#!/usr/bin/env sh

set -ex

git clone https://gitlab.com/jakesys/jn-bootstrap.git /bootstrap
cd /bootstrap
git config --local pull.ff only
git gc
