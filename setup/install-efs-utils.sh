#!/usr/bin/env sh
# Adapted from https://github.com/ajaquith/alpine-apks

set -ex

cd /setup

# get latest efs-utils
git clone https://github.com/aws/efs-utils.git

cd efs-utils

# apply patch
apk add --no-cache patch
patch -s -p1 -i ../patches/efs-utils.patch

# install
mkdir -p /var/log/amazon/efs
install -p -Dm644 -t /etc/amazon/efs dist/efs-utils.conf
install -p -Dm444 -t /etc/amazon/efs dist/efs-utils.crt
install -p -m755 src/watchdog/__init__.py /usr/bin/amazon-efs-mount-watchdog
install -p -m755 src/mount_efs/__init__.py /sbin/mount.efs
install -p -Dm755 -t /etc/init.d ../init.d/amazon-efs-mount-watchdog

# cleanup
cd ..
rm -rf efs-utils
apk del --no-cache patch
