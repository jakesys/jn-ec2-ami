#!/usr/bin/env sh

set -ex

# install the latest aws-cli v2 from the jakesys/aws2:latest docker
# container, which supports both x86_64 and aarch64.

# first we need docker in the build instance
yum update -y
amazon-linux-extras install docker
yum -y install docker
service docker start

# pull the image and run to get version, then copy necessary bits
docker run --name aws2 jakesys/aws2:latest --version
docker cp aws2:/usr/local/aws-cli "$TARGET/usr/local/aws-cli"
ln -s /usr/local/aws-cli/v2/current/bin/aws "$TARGET/usr/local/bin"

service docker stop