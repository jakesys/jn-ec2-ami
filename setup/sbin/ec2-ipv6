#!/usr/bin/env sh
# vim:set ft=sh noet ts=4:

set -e

SCRIPT="$(basename "$0")"

usage() {
	cat <<- EOT
		Usage: $SCRIPT { help | <interface> <action> }
		<action> :-
		  list       - list EC2 IPv6 addresses associated with interface
		  add        - add EC2 IPv6 addresses to interface
		  del        - remove EC2 IPv6 addresses from interface
		  sync       - sync interface IPv6 addreses with EC2
		  install    - config /etc/network/interfaces to auto-add EC2 IPv6
		  uninstall  - remove /etc/network/interfaces auto-add config
	EOT
	if [ "$1" = "help" ]; then
		exit 0
	fi
	exit 1
}

[ "$#" -ne 2 ] && usage "$@"

IFACE="$1"
ACT="$2"

IMDS="X-aws-ec2-metadata-token"
CFG="/etc/network/interfaces"

log() {
	LEV="$1"
	shift
	logger -s -p "kern.$LEV" -t "$SCRIPT" "$@"
}

meta_token() {
	echo -ne "PUT /latest/api/token HTTP/1.0\r\n$IMDS-ttl-seconds: 5\r\n\r\n" |
		nc 169.254.169.254 80 | tail -n 1
}

meta() {
	wget -qO - --header "$IMDS: $(meta_token)" \
		"http://169.254.169.254/latest/meta-data/$1" 2>/dev/null
}

iface_mac() {
	ip -f link addr show "$IFACE" |
		sed -E -e '/ether/!d' \
			-e 's/.*ether ([0-9a-f:]+).*/\1/'
}

iface_ip6s() {
	ip -f inet6 addr show "$IFACE" |
		sed -E -e '/inet6/!d' -e '/inet6 fe80::/d' \
			-e 's/.*inet6 ([0-9a-f:]+).*/\1/'
}

ec2_ip6s() {
	MAC="$(iface_mac)"
	if [ -z "$MAC" ]; then
		log err "Unknown link/ether '$IFACE'"
		exit 1
	fi
	meta "network/interfaces/macs/$MAC/ipv6s"
}

in_list() {
	echo "$2" | grep -Fq "$1"
}

ip6_addr() {
	MSG="$IFACE $1 $2"
	if ip -f inet6 addr "$1" "$2/64" dev "$IFACE" 2>/dev/null; then
		log notice "$MSG - success"
	else
		log err "$MSG - FAILED"
		EXIT=1
	fi
}

cfg_has_iface() {
	grep -Fq "iface $IFACE inet " "$CFG"
}

cfg_has_ec2ipv6() {
	grep -Fq "ec2-ipv6 $IFACE " "$CFG"
}

EXIT=
case "$ACT" in
	list)
		ec2_ip6s
		echo
		;;
	add|del|sync)
		EC2_IP6S="$(ec2_ip6s)"
		IFACE_IP6S="$(iface_ip6s)"
		if [ "$ACT" != "del" ]; then
			for ip6 in $EC2_IP6S; do
				if in_list "$ip6" "$IFACE_IP6S"; then
					[ "$ACT" = "add" ] && log info "$IFACE add $ip6 - already exists"
					continue
				fi
				ip6_addr add "$ip6"
			done
		fi
		if [ "$ACT" != "add" ]; then
			for ip6 in $IFACE_IP6S; do
				[ "$ACT" = "sync" ] && in_list "$ip6" "$EC2_IP6S" && continue
				ip6_addr del "$ip6"
			done
		fi
		;;
	install|uninstall)
		if cfg_has_iface; then
			if [ "$ACT" = "install" ]; then
				if cfg_has_ec2ipv6; then
					log info "already installed for $IFACE in $CFG"
					exit 0
				else
					EXPR="/iface $IFACE inet /a \\\tpost-up /sbin/ec2-ipv6 $IFACE add\n\tpre-down /sbin/ec2-ipv6 $IFACE del"
				fi
			else
				EXPR="/ec2-ipv6 $IFACE /d"
			fi
			sed -i "$EXPR" "$CFG"
			log notice "${ACT}ed for $IFACE in $CFG"
		else
			log error "'iface $IFACE inet' not defined in $CFG"
			exit 1
		fi
		;;
	*)
		log error "Invalid '$ACT' action" >&2
		usage
		;;
esac
exit $EXIT
