#!/usr/bin/env sh

set -ex

cd /setup

# add EC2 user to docker group
addgroup "$EC2_USER" docker

# so that docker.log doesn't grow without bound...

# wrap dockerd in log_proxy_wrapper...
mkdir -p /var/log/log_proxy
cp /etc/conf.d/docker /etc/conf.d/docker.orig
cat >/etc/conf.d/docker <<EOF
DOCKERD_BINARY="/usr/bin/log_proxy_wrapper"
DOCKER_OPTS="-F /var/log/log_proxy -O /var/log/docker.log -s 10485760 -t 0 -- /usr/bin/dockerd"
EOF
# ...10 MiB max, save 5 rotated logs, don't rotate daily

# update docker init.d so supervise_daemon doesn't try handling logs
sed -i '/^supervise_daemon_args=/d' /etc/init.d/docker
